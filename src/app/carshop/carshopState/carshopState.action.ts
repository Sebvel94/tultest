import { createAction, props } from "@ngrx/store";

export const carshopState = createAction('[UpdateState] UpdateState',
  props<{ currentState: string }>())
