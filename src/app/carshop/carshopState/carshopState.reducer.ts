import { createReducer, on, Action } from "@ngrx/store";
import { carshopState } from "./carshopState.action";


const initilState = 'Pending';

const _updateCurrentState = createReducer(
  initilState,
  on(carshopState, (state, {currentState}) => currentState)

)

export function updateCurrenttate(state, action: Action) {
  return _updateCurrentState(state, action);

}
