import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Carshop } from '../dashboard/carshop.reducers';
import * as carsShopActions from '../dashboard/carshop/carshop.actions';
import { CarState } from '../dashboard/carshop/carshop.reducer';
import { UserService } from '../services/user.service';
import { UpdateCarshopState } from './CarshopState.redux';
import { carshopState } from './carshopState/carshopState.action';

@Component({
  selector: 'app-carshop',
  templateUrl: './carshop.component.html',
  styleUrls: ['./carshop.component.css']
})
export class CarshopComponent implements OnInit {
  shoppingCar;
  shopSubscription: Subscription;
  productStateSub: Subscription;
  currentStateSub: Subscription;
  currentState: string;
  newRequest: boolean = false;
  payedRequest: boolean = false;
  savedRequest: boolean = false;
  constructor(
    private _carshopStore: Store<Carshop>,
    private _carshopInitState: Store<UpdateCarshopState>,
    private _userService: UserService,
    private _router: Router

  ) { }

  ngOnInit(): void {
    this.shopSubscription = this._carshopStore.select('carShop').subscribe(shoppingCar => {
      this.shoppingCar = shoppingCar.products;
    });

    this.currentStateSub = this._carshopInitState.select('updateState').subscribe(carshopState => {
      this.currentState = carshopState;
    });

    this.verifySession();
  }

  removeItem(product) {
    if (product.cuantity > 1) {
      this._carshopStore.dispatch(carsShopActions.removeProduct({ product }));
    } else {
      this._carshopStore.dispatch(carsShopActions.deleteProduct({ product }));
    }
  }

  addItem(product) {
    this._carshopStore.dispatch(carsShopActions.incrementProduct({ product }));
  }


  createCar() {
    this._userService.createCar();
    this.savedRequest = true;
    this.payedRequest = true;
  }

  createProductCar() {
    this._userService.createProductCar(this.shoppingCar);
    this._userService.updateProductState();
    this.newRequest = true;
    this.payedRequest = false;
  }

  resetCar() {
    this._carshopStore.dispatch(carsShopActions.resetProducts());
    this._carshopInitState.dispatch(carshopState({ currentState: 'Pending' }));
    this.newRequest = false;
    this.payedRequest = false;
    this.savedRequest = false;
    this._router.navigate(['dashboard'])
  }

  verifySession() {
    this._userService.initAuthState();
  }
}
