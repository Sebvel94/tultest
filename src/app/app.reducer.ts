import { ActionReducerMap } from '@ngrx/store';
import { loginReducer } from './auth/session/session.reducer';
import { addProductReducer } from './dashboard/carshop/carshop.reducer';
import * as ui from './shadred/ui.reducers';
import * as auth from './auth/auth.reducer';
import { updateCurrenttate } from './carshop/carshopState/carshopState.reducer';


export interface AppState {
   ui: ui.State
}



export const appReducer = {
   ui: ui.uiReducer,
   email: loginReducer,
   carShop: addProductReducer,
   manageUser: auth.authReducer,
   updateState: updateCurrenttate,
}

