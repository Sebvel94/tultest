import { createAction, props } from "@ngrx/store";


export const incrementProduct = createAction('[Carshop] IncrementProduct',
props<{product: any}>());
export const addProduct = createAction('[Carshop] AddProduct',
  props<{ product: any }>()
);
export const removeProduct = createAction('[Carshop] RemoveProduct',
  props<{ product: any }>());

export const deleteProduct = createAction('[Carshop] DeleteProduct',
  props<{ product: any }>());

export const resetProducts = createAction('[Carshop] ResetProducts')
