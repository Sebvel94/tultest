import { Action, createReducer, on } from "@ngrx/store";
import { Carshop } from "../carshop.reducers";
import { addProduct, deleteProduct, incrementProduct, removeProduct, resetProducts } from "./carshop.actions";
export const carCuantity = 0
export const products = [];
export const initialState = {
  carCuantity: 0,
  products: []
}
export interface CarState {
  product: Carshop,
  cuantiy: number
}
const _addProductReducer = createReducer(
  initialState,
  on(addProduct, (state, { product }) => ({
    ...state,
    carCuantity: state.carCuantity + 1,
    products: [...state.products, { ...product, cuantity: product.cuantity + 1 }]

  })),

  on(removeProduct, (state, { product }) => ({
    ...state,
    carCuantity: state.carCuantity - 1,
    products: state.products.map(productsMap => ({ ...productsMap }))
      .map(productsMap => {
        if (productsMap.id == product.id) {
          return {
            ...productsMap,
            cuantity: productsMap.cuantity - 1
          }
        } else {
          return productsMap;
        }
      })
  }
  )),

  on(deleteProduct, (state, { product }) => ({
    ...state,
    carCuantity: state.carCuantity - 1,
    products: [...state.products.filter(productF => {
      return productF.id !== product.id
    })]
  })),

  on(incrementProduct, (state, { product }) => ({
    ...state,
    carCuantity: state.carCuantity + 1,
    products: state.products
      .map(productsMap => {
        if (productsMap.id == product.id) {
          return {
            ...productsMap,
            cuantity: (productsMap.cuantity + 1)
          }
        } else {
          return productsMap;
        }
      })
  })),

  on(resetProducts, state => ({
    ...state,
    carCuantity: 0,
    products: []

  }))
)

export function addProductReducer(state, action: Action) {
  return _addProductReducer(state, action);
}
