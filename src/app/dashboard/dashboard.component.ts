import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { Carshop } from './carshop.reducers';
import * as carRedux from './carshop/carshop.actions';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  prodFirebase;
  carshopProducts = [];
  getProductSubs: Subscription
  constructor(
    private _messaService: MessagesService,
    private _carShopStore: Store<Carshop>,
    private _userService: UserService
  ) { }

  ngOnInit(): void {
    this.getProductos()
    this.createSubscripion();
    this.verifySession();
  }

  createSubscripion() {
    if (!this.getProductSubs) {
      this.getProductSubs = this._carShopStore.select('carShop').subscribe(productList => {
        this.carshopProducts = productList.products;
      });
    }
  }

  addProduct(productR) {
    const producExists = this.carshopProducts.find(productFind => {
      return (productR.id == productFind.id)
    });

    if (producExists) {
      this._carShopStore.dispatch(carRedux.incrementProduct({ product: productR }));
    } else {
      this._carShopStore.dispatch(carRedux.addProduct({ product: productR }));
    }
  }

  getProductos() {
    this._userService.getProducts().valueChanges().subscribe((data: any) => {
      this.prodFirebase = data.products;
    });
  }


  formatProducts(products) {
    for (let item in products) {
      this.prodFirebase.push(products[item])
    }
  }

  ngOnDestroy() {
    this.getProductSubs?.unsubscribe();
  }

  verifySession() {
    this._userService.initAuthState();
  }
}
