import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from './app.reducer';
import { SessionState } from './auth/session.reducers';
import { logout } from './auth/session/session.actions';
import { Carshop } from './dashboard/carshop.reducers';
import { resetProducts } from './dashboard/carshop/carshop.actions';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'in-out-app';
  emailSession: string;
  productCuantity: number;
  isLoading = false;
  constructor(
    private store: Store<SessionState>,
    private productStore: Store<Carshop>,
    private authStore: Store<AppState>,
    public _router: Router,
    private _userService: UserService,
  ){
    this._userService.initAuthState();
  }

  ngOnInit(): void {
    this.store.select('email').subscribe(email => {
     this.emailSession = email;
    });

    this.productStore.select('carShop').subscribe(cuantity => {
      this.productCuantity = cuantity.carCuantity;
    });
    this.authStore.select('ui').subscribe( ui => {
      this.isLoading = ui.isLoading;
    });
  }

  logout() {
    this.store.dispatch(logout());
    this.productStore.dispatch(resetProducts());
    this._userService.signOut();
    this._router.navigate(['']);
  }

  shoppingCar() {
    this._router.navigate(['/shoppingCar'])
  }

  goDashboard() {
    this._router.navigate(['/dashboard']);
  }
}
