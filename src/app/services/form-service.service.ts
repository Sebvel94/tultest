import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormServiceService {

  constructor() { }

  getError(field: string, formGroup: FormGroup) {
    if (formGroup.get(field).hasError('required')) {
      return 'El campo es requerido';
    }
    else if (formGroup.get(field).hasError('pattern')) {
      return 'Los caracteres ingresados no son válidos para este campo';
    }
    else if (formGroup.get(field).hasError('min')) {
      return 'El valor está por debajo del valor mínimo permitido';
    }
    else if (formGroup.get(field).hasError('max')) {
      return 'El valor ingresado supera el valor máximo permitido';
    }
   else  if (formGroup.get(field).hasError('minlength')) {
      return 'El campo tiene menos caracteres de los requeridos';
    }
    else if (formGroup.get(field).hasError('maxlength')) {
      return 'El campo supera el número máximo de caracteres permitidos';
    }
    else if (formGroup.get(field).hasError('email')) {
      return `El formato para ${field} es incorrecto`;
    } else if(formGroup.get(field).hasError('notMatch')){
      return 'Las contraseñas no coinciden';
    }

    else {
      return true;
    }
  }
}
