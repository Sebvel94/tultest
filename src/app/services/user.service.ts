import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { User } from '../models/userModel';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { AppState } from '../app.reducer';
import { Store } from '@ngrx/store';
import * as authF from '../auth/auth.actions';
import { Subscription } from 'rxjs';
import { Car } from '../models/car';
import { UpdateCarshopState } from '../carshop/CarshopState.redux';
import { updateCurrenttate } from '../carshop/carshopState/carshopState.reducer';
import { carshopState } from '../carshop/carshopState/carshopState.action';




@Injectable({
  providedIn: 'root'
})
export class UserService {
  registerUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=';
  loginUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=';
  productsUrl = 'https://tultest-default-rtdb.firebaseio.com/products';
  productCollectionId = 'FvJb3vno1oT61kD5zbKH';
  database = firebase.database;
  APIKEY = environment.firebase.apiKey;
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' }) };
  getFireStoreUserSub: Subscription;
  carId = 0;
  currentUser;
  currentCar;
  currentObjectId;

  constructor(
    private _http: HttpClient,
    public auth: AngularFireAuth,
    private _fireStore: AngularFirestore,
    private _store: Store<AppState>,
    private _carshopInitState: Store<UpdateCarshopState>,

  ) { }

  registerUser(userData) {
    const { email, password } = userData;
    return this.auth.createUserWithEmailAndPassword(email, password)
      .then(({ user }) => {
        const newUser = new User(user.uid, userData.name, user.email);
        return this._fireStore.doc(`${newUser.uid}/user`)
          .set({ ...newUser })
      });
  }

  login(userData) {
    const { email, password } = userData;
    const currentUser = this.auth.signInWithEmailAndPassword(email, password);
    this.initAuthState();
    sessionStorage.setItem('emailSession', email)
    return currentUser;
  }

  async createCar() {
    this.currentCar = new Car(`${this.currentUser.uid}${this.carId}`, 'pending')
    this.carId++;
    this.currentObjectId = await this._fireStore.doc(`${this.currentUser.uid}/car`).collection('cars').add({ ...this.currentCar });

  }

  updateProductState() {
    this._fireStore.doc(`${this.currentUser.uid}/car/cars/${this.currentObjectId.id}`).update({ status: 'complete' });
    this._carshopInitState.dispatch(carshopState({ currentState: 'complete' }));

  }

  createProductCar(productsCar) {
    let products = [];
    productsCar.forEach(element => {
      const register = {
        product_id: element.id,
        car_id: this.currentCar.id,
        quantity: element.cuantity
      }
      products.push(register);
    });
    this._fireStore.doc(`${this.currentUser.uid}/productCar`)
      .collection('productsCar').add({ products })
  }

  signOut() {
    sessionStorage.removeItem('emailSession')
    this.auth.signOut();
    this._store.dispatch(authF.unsetUser());
  }

  getProducts() {
    return this._fireStore.doc(`products/${this.productCollectionId}`);
  }

  initAuthState() {
    this.auth.authState.subscribe(fbUser => {
      if (fbUser) {
        if (!this.getFireStoreUserSub) {
          this.getFireStoreUserSub = this._fireStore.doc(`${fbUser.uid}/user`).valueChanges().subscribe((fbDbuser: any) => {
            this.currentUser = User.fromFireBase(fbDbuser);
            this._store.dispatch(authF.setUser({ user: this.currentUser }));
          })
        }
      }
    });
  }

  isAuth() {
    return this.auth.authState.pipe(
      map(fbUser => fbUser != null)
    );
  }

}
