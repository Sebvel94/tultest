import { Action, createReducer, on } from "@ngrx/store";
import { login, logout } from "./session.actions";
export const loginState= '' ;

const _loginReduer = createReducer (
  on(login, (state, {email}) => state = email),
  on(logout, (state) => state= undefined)
);

export function loginReducer(state, action: Action) {
  return _loginReduer(state, action)
}
