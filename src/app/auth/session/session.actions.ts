import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Login] login',
  props<{email:string}>()
  );

export const logout = createAction('[Loing] logout')
