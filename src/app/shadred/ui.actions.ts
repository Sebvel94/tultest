import { createAction } from '@ngrx/store';

export const isLoadiing = createAction('[UI Component] isLoadiing');
export const stopLoading = createAction('[UI Component] stopLoading');
