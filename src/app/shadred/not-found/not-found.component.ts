import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {
  isLoggin;
  urlNavigate;

  constructor(
    private _userService: UserService,
    private router: Router,

  ) { }

  ngOnInit(): void {
    this.isLoggin = sessionStorage.getItem('emailSession');
    this.verifySession();

  }

  verifySession() {
    this.urlNavigate = sessionStorage.getItem('emailSession') ? '/dashboard' : '';
  }

  moveRoute() {
    this.router.navigate([this.urlNavigate])
  }


}
